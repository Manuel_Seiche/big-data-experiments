package de.mseiche.retrieval;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.IntStream;

/**
 * Das Vector-Space-Modell ist ein generisches Information Retrieval Model.
 * Davon abgeleitet ist z. B. das Bag of Visual Words - Modell.
 * 
 * @author Manuel Seiche
 * @since 06.02.2019
 * @param <D> Typ der verwalteten {@link Document Dokumente} (z. B. eine
 *        Buchkategorie)
 * @param <I> Typ der Index-Terme
 */
public abstract class VectorSpaceModel<D extends Document<I>, I> {

	/**
	 * Abbildung jedes Dokument zu einem korrespondierenden Document-Weight-Vector
	 */
	private final Map<D, double[]> documents;

	public VectorSpaceModel(final Set<D> documents) {
		super();
		this.documents = new HashMap<>();
		documents.forEach(doc -> this.documents.put(doc, null));
	}

	/**
	 * Initialisiert das Weighting (Abh&auml;ngig von der Implementierung des
	 * spezifischen Vector-Space-Modells)
	 */
	public final void initializeWeights() {
		for (final D document : documents.keySet()) {
			documents.put(document, computeWeightVector(document));
		}
	}

	public final D singleResult(final boolean[] query) {
		D nearestDocument = null;
		double maximalScore = Double.MIN_VALUE;
		for (final Entry<D, double[]> documentAndWeight : documents.entrySet()) {
			final double score = score(query, documentAndWeight.getValue());
			if (score > maximalScore) {
				maximalScore = score;
				nearestDocument = documentAndWeight.getKey();
			}
		}
		return nearestDocument;
	}

	public double score(final boolean[] query, final double[] objectVector) {
		return IntStream.range(0, query.length)//
				.filter(i -> query[i])//
				.mapToDouble(i -> objectVector[i])//
				.sum();
	}

	public final Map<D, Double> scoreMap(final boolean[] query) {
		final Map<D, Double> scoreMap = new HashMap<>();

		for (final Entry<D, double[]> documentAndWeight : documents.entrySet()) {
			final double score = score(query, documentAndWeight.getValue());
			scoreMap.put(documentAndWeight.getKey(), score);
		}

		return scoreMap;
	}

	/**
	 * Berechnet den Gewichtsvektor f&uuml;r ein Dokument
	 * 
	 * @param document Zieldokument
	 * @return Gewichtsvektor des Dokuments
	 */
	protected abstract double[] computeWeightVector(D document);

	/**
	 * @return {@link #documents}
	 */
	public final Map<D, double[]> getDocuments() {
		return documents;
	}
}
