package de.mseiche.retrieval.preprocessing;

import java.util.Collection;
import java.util.stream.Collectors;

import de.mseiche.retrieval.Document;

/**
 * F&uuml;hrt ein grobes textuelles Preprocessing ohne spezifischere Selektion
 * durch.
 * 
 * @author Manuel Seiche
 * @since 10.02.2019
 *
 * @param <I> Typ der Index-Terme
 */
public final class DocumentPreprocessor {

	public final void preprocess(final Collection<? extends Document<String>> documents) {
		lexicalAnalysis(documents);
		eliminateStopWords(documents);
		stemming(documents);
	}

	private void lexicalAnalysis(Collection<? extends Document<String>> documents) {
		documents.stream()//
				.forEach(document -> document.getTerms(//
						document.getTerms()//
								.stream()//
								.map(t -> t.replaceAll("\\,\\.\\-\\s", ""))//
								.collect(Collectors.toList())));
	}

	private void eliminateStopWords(final Collection<? extends Document<String>> documents) {
		documents.stream()//
				.forEach(document -> document.getTerms(//
						document.getTerms()//
								.stream()//
								.filter(t -> !isStopWord(documents, t))//
								.collect(Collectors.toList())));
	}

	private boolean isStopWord(final Collection<? extends Document<String>> documents, final String indexTerm) {
		final double documentFrequency = documents.stream()//
				.filter(d -> d.getTerms().contains(indexTerm))//
				.count();
		final double ratio = documentFrequency / documents.size();
		return ratio >= 0.8D;
	}

	private void stemming(final Collection<? extends Document<String>> documents) {
		// TODO
	}
}
