package de.mseiche.retrieval.bow;

import java.util.Set;

import de.mseiche.retrieval.Document;
import de.mseiche.retrieval.VectorSpaceModel;
import de.mseiche.retrieval.weighting.TermWeighting;

/**
 * Der BoW-Approach ist eine Spezifikation des {@link VectorSpaceModel
 * Vector-Space-Modells}
 * 
 * @author Manuel Seiche
 * @since 06.02.2019
 *
 * @param <D> Typ der verwalteten Dokumente (z. B. eine Buchkategorie)
 * @param <I> Typ der Index-Terme
 */
public final class BagOfWords<D extends Document<I>, I> extends VectorSpaceModel<D, I> {

	/**
	 * Das Vokabular an W&ouml;rtern (z. B. Schlagw&ouml;rter in
	 * Buchbeschreibungen), welches zur Berechnung der Gewichte verwendet wird.
	 */
	private Vocabulary<I> vocabulary;

	/**
	 * Das {@link TermWeighting}, welches zur Berechnung der Gewichte von
	 * Index-Termen in ihrem zugeh&ouml;rigen Dokument verwendet wird.
	 */
	private TermWeighting<D, I> termWeighting;

	/**
	 * @param documents  {@link #getDocuments()}
	 * @param vocabulary {@link #termWeighting}
	 */
	public BagOfWords(final Set<D> documents, final Vocabulary<I> vocabulary, final TermWeighting<D, I> termWeighting) {
		super(documents);
		this.vocabulary = vocabulary;
		this.termWeighting = termWeighting;
	}

	@Override
	protected final double[] computeWeightVector(final D document) {
		return vocabulary.getIndexTerms()//
				.stream()//
				.mapToDouble(indexTerm -> //
				document.getTerms().contains(indexTerm) ? termWeighting.getTermWeight(indexTerm, document) : 0D)//
				.toArray();
	}

	/**
	 * @return {@link #vocabulary}
	 */
	public final Vocabulary<I> getVocabulary() {
		return vocabulary;
	}
}
