package de.mseiche.retrieval.bow;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import de.mseiche.retrieval.Document;

/**
 * Ein {@link BagOfWords} h&auml;lt genau ein {@link Vocabulary} mit
 * Index-Termen
 * 
 * @author Manuel Seiche
 * @since 10.02.2019
 * @param <I> Typ der Index-Terme
 */
public final class Vocabulary<I> {

	/**
	 * Die Index-Terme (Reihenfolge wichtig!)
	 */
	private List<I> indexTerms;

	/**
	 * Erzeugt ein {@link Vocabulary} auf Basis einer Menge von {@link Document
	 * Dokumenten}. Die {@link #indexTerms} werden mit der Vereinigung aller
	 * Index-Terme der Dokumente initialisiert.
	 * 
	 * @param documents Menge aller {@link Document Dokumente}
	 */
	public Vocabulary(final Set<? extends Document<I>> documents) {
		this(documents.stream()//
				.flatMap(d -> d.getTerms().stream())//
				.collect(Collectors.toList()));
	}

	/**
	 * Erzeugt ein {@link Vocabulary} mit bereits festgelegten Index-Termen
	 * 
	 * @param indexTerms {@link #indexTerms}
	 */
	public Vocabulary(final List<I> indexTerms) {
		super();
		this.indexTerms = indexTerms.stream().distinct().collect(Collectors.toList());
	}

	public boolean[] createQuery(final Collection<I> queryTerms) {
		final boolean[] query = new boolean[indexTerms.size()];
		for (int termIndex = 0; termIndex < indexTerms.size(); termIndex++) {
			final I indexTerm = indexTerms.get(termIndex);
			query[termIndex] = queryTerms.stream().anyMatch(queryTerm -> Objects.equals(queryTerm, indexTerm));
		}
		return query;
	}

	/**
	 * @return {@link #indexTerms}
	 */
	public List<I> getIndexTerms() {
		return indexTerms;
	}
}
