package de.mseiche.retrieval;

import java.util.List;

/**
 * Ein Dokument, welches durch ein Retrieval Modell anhand von Index-Termen auf
 * Basis eines entsprechenden Query-Objekts erhalten werden kann.
 * 
 * @author Manuel Seiche
 * @since 06.02.2019
 *
 * @param <I> Typ der in {@link #terms} gehaltenen Terme (z. B. Schlagworte
 *        in Buchbeschreibungen)
 */
public class Document<I> {

	private List<I> terms;

	public Document(final List<I> terms) {
		super();
		this.terms = terms;
	}

	public final void getTerms(final List<I> indexTerms) {
		this.terms = indexTerms;
	}

	/**
	 * @return {@link #terms}
	 */
	public final List<I> getTerms() {
		return terms;
	}
}
