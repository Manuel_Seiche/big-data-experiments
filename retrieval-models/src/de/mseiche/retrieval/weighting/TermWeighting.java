package de.mseiche.retrieval.weighting;

import de.mseiche.retrieval.Document;

/**
 * Index-Terme k&ouml;nnen auf unterschiedliche Weise gewichtet werden. Dieses
 * Interface muss von unterschiedlichen Gewichtungsmethodiken implementiert
 * werden (z. B. tf-idf)
 * 
 * @author Manuel Seiche
 * @since 06.02.2019
 * 
 * @param <D> Typ der verwalteten {@link Document Dokumente} (z. B. eine
 *        Buchkategorie)
 * @param <I> Typ der Index-Terme
 * 
 * @see TFIDF
 */
@FunctionalInterface
public interface TermWeighting<D extends Document<I>, I> {

	Double getTermWeight(I indexTerm, D document);
}
