package de.mseiche.retrieval.weighting;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import de.mseiche.retrieval.Document;

/**
 * Weist einem Index-Term ein Gewicht mittels tf-idf-Term-Weighting zu:<br>
 * <ul>
 * <li>tf_i_d = Term Frequency = Anzahl der Vorkommen eines Index-Terms i im
 * Dokument d</li>
 * <li>df_i = Document Frequency = Anzahl der Dokumente d, welche den Index-Term
 * i enthalten</li>
 * <li>idf_i_d = Inverse Document Frequency = log(|D|/df_i)</li>
 * <li><b>tf_idf_i_d = tf_i_d * idf_i_d</b></li>
 * </ul>
 * 
 * @author Manuel Seiche
 * @since 06.02.2019
 * 
 * @param <D> Typ der verwalteten Dokumente (z. B. eine Buchkategorie)
 * @param <I> Typ der Index-Terme
 */
public final class TFIDF<D extends Document<I>, I> implements TermWeighting<D, I> {

	/**
	 * idf_i_d kann global einmalig berechnet werden. Daf&uuml;r dient diese
	 * {@link Map}
	 */
	private final Map<I, Double> inverseDocumentFrequencies;

	public TFIDF(final Set<D> documents, final List<I> indexTerms) {
		super();
		inverseDocumentFrequencies = new HashMap<>();
		initializeIDF(documents, indexTerms);
	}

	@Override
	public Double getTermWeight(I indexTerm, D document) {
		final double idf = inverseDocumentFrequencies.get(indexTerm);
		if (idf == 0) {
			return 0D;
		}
		final double tf = computeTermFrequency(indexTerm, document);
		return tf * idf;
	}

	private double computeTermFrequency(final I indexTerm, final D document) {
		return document.getTerms().stream()//
				.filter(i -> Objects.equals(i, indexTerm))//
				.count();
	}

	private void initializeIDF(final Set<D> documents, final List<I> indexTerms) {
		indexTerms.forEach(indexTerm -> {
			final double documentFrequency = computeDocumentFrequency(indexTerm, documents);
			final double inverseDocumentFrequency = Math.log10((double) documents.size() / documentFrequency);
			inverseDocumentFrequencies.put(indexTerm, inverseDocumentFrequency);
		});
	}

	private double computeDocumentFrequency(final I indexTerm, final Set<D> documents) {
		return documents.stream()//
				.map(Document::getTerms)//
				.filter(terms -> terms.contains(indexTerm))//
				.count();
	}
}
