package de.mseiche.retrieval.bookcategories;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import de.mseiche.retrieval.bow.BagOfWords;
import de.mseiche.retrieval.bow.Vocabulary;
import de.mseiche.retrieval.preprocessing.DocumentPreprocessor;
import de.mseiche.retrieval.weighting.TFIDF;

/**
 * Vollst&auml;ndiges Beispiel f&uuml;r die Anwendung des
 * {@link BagOfWords}-Approach mit {@link TFIDF}-Term Weighting.
 * 
 * @author Manuel Seiche
 * @since 06.02.2019
 */
public final class BookCategoryExample {

	public static void main(final String[] args) {
		/*
		 * Ein paar Buchkategorien mit Beispiel-Texten instanziieren. Je mehr, desto
		 * besser! (Im Normalfall sollte das natuerlich generisch auf Basis einer
		 * Buch-Datenbank geschehen)
		 */
		final BookCategory krimi = new BookCategory("Krimi", extractQueryTerms(//
				"Detektiv Heinz untersucht einen brutalen Mord", //
				"Serienkiller Olaf bringt zahlreiche Personen und dann sich selbst um", //
				"Polizist Erwin untersucht einen Mord an einer Blondine und ihrem Liebhaber"));
		final BookCategory romanze = new BookCategory("Romanze", extractQueryTerms(//
				"Romeo und Julia: Eine gro�e Liebe", //
				"Ein Herz nach dem anderen wird von Casanova Bert gebrochen", //
				"Romantisch und noch mehr romantisch... und ganz viel Liebe. Richtig kitschig."));
		final BookCategory historisch = new BookCategory("Historisch", extractQueryTerms(//
				"Julius C�sar wird im Senatsgeb�ude von mehreren Schwertern und Dolchen durchbohrt", //
				"Atilla der Hunne zieht in die Schlacht", //
				"Von der Entdeckung des ersten Stammes der Indianer"));
		// Extrem grobes Preprocessing
		preprocess(krimi, romanze, historisch);

		final Set<BookCategory> documents = Stream.of(krimi, romanze, historisch).collect(Collectors.toSet());
		final Vocabulary<String> vocabulary = new Vocabulary<>(documents);
		final TFIDF<BookCategory, String> tfIdf = new TFIDF<>(documents, vocabulary.getIndexTerms());

		// Das Retrieval Model initialisieren, welches uns Vorhersagen ausspucken soll
		final BagOfWords<BookCategory, String> retrievalModel = new BagOfWords<>(documents, vocabulary, tfIdf);
		retrievalModel.initializeWeights();

		// Ein paar Beispiele (sorgen fuer Bildschirmausgaben)
		example("Detektiv Hans untersucht in diesem Roman einen Mord aus Liebe", retrievalModel);
		example("Bl�mchen und Rosen. Die gro�e Liebe. (Vorsicht: romantisch)", retrievalModel);
		example("Die Entdeckung des Jahrtausends!", retrievalModel);
	}

	private static void example(final String description, final BagOfWords<BookCategory, String> retrievalModel) {
		final boolean[] query = retrievalModel.getVocabulary().createQuery(extractQueryTerms(description));
		final BookCategory result = retrievalModel.singleResult(query);
		System.out.println('[' + result.getName() + "] <- \"" + description + '\"');
		final Map<BookCategory, Double> ranking = retrievalModel.scoreMap(query);
		final AtomicInteger rank = new AtomicInteger();
		System.out.println(ranking.keySet().stream()//
				.sorted((c1, c2) -> Double.compare(ranking.get(c2), ranking.get(c1)))//
				.map(c -> "\t" + rank.incrementAndGet() + ". Platz: " + c.getName() + " -> " + ranking.get(c))//
				.collect(Collectors.joining("\n")));
	}

	private static void preprocess(final BookCategory... bookCategories) {
		new DocumentPreprocessor().preprocess(Arrays.asList(bookCategories));
	}

	private static List<String> extractQueryTerms(final String... descriptions) {
		return Arrays.stream(descriptions).flatMap(BookCategoryExample::queryTerms).collect(Collectors.toList());
	}

	private static Stream<String> queryTerms(final String description) {
		return Arrays.stream(description.split("\\s+")).map(String::trim).map(String::toLowerCase);
	}
}
