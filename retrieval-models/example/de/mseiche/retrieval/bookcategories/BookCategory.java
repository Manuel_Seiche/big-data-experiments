package de.mseiche.retrieval.bookcategories;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import de.mseiche.retrieval.Document;
import de.mseiche.retrieval.bow.BagOfWords;

/**
 * {@link Document}, welches im Rahmen eines {@link BagOfWords} durch
 * &Uuml;bergabe von Schlagworten erhalten werden soll.
 * 
 * @author Manuel Seiche
 * @since 06.02.2019
 */
public final class BookCategory extends Document<String> {

	/**
	 * Name der Buchkategorie (z. B. "Krimi")
	 */
	private final String name;

	/**
	 * @param name       {@link #name}
	 * @param indexTerms {@link #getTerms()}
	 */
	public BookCategory(final String name, final String... indexTerms) {
		this(name, Arrays.asList(indexTerms));
	}

	/**
	 * @param name       {@link #name}
	 * @param indexTerms {@link #getTerms()}
	 */
	public BookCategory(final String name, final List<String> indexTerms) {
		super(indexTerms);
		this.name = name;
	}

	/**
	 * @return {@link #name}
	 */
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return getName();
	}

	public static BookCategory queryByDescription(final String description) {
		return new BookCategory(null,
				Arrays.stream(description.split("\\s+")).map(String::trim).collect(Collectors.toList()));
	}

	public static BookCategory queryByTerms(final String... indexTerms) {
		return new BookCategory(null, indexTerms);
	}
}
